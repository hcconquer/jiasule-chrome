function saveOptions(options) {
	var defopts = {
		"show_google": true,
		"show_baidu": true,
		"show_sogou": true,
		"show_soso": true,
		"show_so": true
	};
	var opts = options || defopts;
	chrome.storage.sync.set({
		'show_google': opts['show_google'],
		'show_baidu': opts['show_baidu'],
		'show_sogou': opts['show_sogou'],
		'show_soso': opts['show_soso'],
		'show_so': opts['show_so']
	}, function() {
	});
}

function reportFeedback(fbi) {
    $.post("http://stat.zheezes.com/stat/rest/feedback",
        fbi, function(data, status, jqxhr) {
            console.debug(data);
        }
    );
}
