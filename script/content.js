(function(window) {

console.debug('jiasule start');

function setSearchState(sid, checked) {
	// console.debug('%s %d %s', sid.attr('id'), sid.children().length, checked);
	if (sid.children().length > 0) {
		if (!checked) {
			sid.click();
		}
	} else {
		if (checked) {
			sid.click();
		}
	}
}

function fixs() {
	chrome.storage.sync.get(null, function(item) {
		setSearchState($('#legend_google'), item['show_google']);
		setSearchState($('#legend_baidu'), item['show_baidu']);
		setSearchState($('#legend_sogou'), item['show_sogou']);
		setSearchState($('#legend_soso'), item['show_soso']);
		setSearchState($('#legend_so'), item['show_so']);
	});
}

$(function() {
	fixs();
});

})(window);
