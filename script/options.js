$(function() {

console.log('options start');

var manifest = chrome.runtime.getManifest();

function initpage() {
	var manifest = chrome.runtime.getManifest();
	$('title').text(chrome.i18n.getMessage('title'));
	$('#version').text(manifest.version);
}

chrome.storage.sync.get(null, function(item) {
	$('#show_google').prop('checked', item['show_google']);
	$('#show_baidu').prop('checked', item['show_baidu']);
	$('#show_sogou').prop('checked', item['show_sogou']);
	$('#show_soso').prop('checked', item['show_soso']);
	$('#show_so').prop('checked', item['show_so']);
});

$('#saveopt').on('click', function() {
	chrome.storage.sync.set({
		'show_google': $('#show_google').prop('checked'),
		'show_baidu': $('#show_baidu').prop('checked'),
		'show_sogou': $('#show_sogou').prop('checked'),
		'show_soso': $('#show_soso').prop('checked'),
		'show_so': $('#show_so').prop('checked')
	}, function() {
	});
});

$('#fbsub').on('click', function() {
	reportFeedback({
		'appname': 'jiasule-chrome',
		'appver': manifest['version'],
		'fbtext': $('#fbtext').val()
	});
});

initpage();

});

