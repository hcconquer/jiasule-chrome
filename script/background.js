(function(window) {

console.log('background start');

// var manifest = chrome.runtime.getManifest();

function showOptionsPage(tab) {
	var manifest = chrome.runtime.getManifest();
	var url = chrome.runtime.getURL(manifest.options_page);
	if (tab) {
		chrome.tabs.update(tab.id, {
			"url": url
		});
	} else {
		chrome.tabs.create(
			{
				"url": url
			}, 
			function(ntab) {
			}
		);
	}
}

function checkShowOptionsPage() {
	var manifest = chrome.runtime.getManifest();
	chrome.storage.sync.get(['version'], function(item) {
		var chg = 0; // not change
		if (item['version']) {
			if (manifest.version > item['version']) {
				// console.debug('update');
				chg = 1; // update
			} else if (manifest.version < item['version']) {
				chg = -1;
			}
		} else {
			// console.debug('install');
			chg = 2; // full new install
			saveOptions(null); // use default
		}
		console.debug("oldver: %s, newver: %s, chg: %d", item['version'], manifest.version, chg);
		if (chg != 0) {
			var url = chrome.runtime.getURL(manifest.options_page);
			chrome.tabs.create(
				{
					"url": url
				}, 
				function(tab) {
				}
			);
			chrome.storage.sync.set(
				{
					'version': manifest.version
				}, 
				function() {
				}
			);
		}
	});
}

/*
 * used for set popup, title, menu e.g.
 */
function setCtxt(param) {
	console.debug('set ctxt, param: %s', JSON.stringify(param));
	if ((param) && (!$.isEmptyObject(param))) {
		var popup = '';
		if (param.authed) {
			popup = 'popup.html';
		}
		chrome.browserAction.setPopup({
			"popup": popup
		});
		
		var title = '';
		if ((param.authid) && (param.authid > 0)) {
			title = chrome.i18n.getMessage('title_user', [ param.authid ]);
		} else {
			var manifest = chrome.runtime.getManifest();
			title = chrome.i18n.getMessage('title_version', [ manifest.version ]);
		}
		chrome.browserAction.setTitle({
			"title": title
		});
	} else {
		/*
		checkPassAuth({}, function(data, status) {
			if (data && data['data'] && data.data['authid']) {
				setCtxt({ "authed": true, "authid": data.data['authid'] });
			} else {
				setCtxt({ "authed": false });
			}
		});
		*/
		setCtxt({ "authed": false });
	}
}

function tabRemovedListener(tabId, info) {
}

function windowCreatedListener(window) {
	setCtxt({}); // when extension reload, need to check and set
	// $('title').text(chrome.i18n.getMessage('title'));
}

function windowRemovedListener(winId) {
}

function popupClickedListener(tab) {
	chrome.tabs.query({
		"url": "*://*.jiasule.com/*"
	}, function(tabs) {
		if (tabs.length > 0) { // cmpass tab opened
			var umctab = tabs[0];
			chrome.tabs.query({
				"active": true
			}, function(tabs) {
				var curtab = tabs[0];
				if (umctab.id == curtab.id) { // current is jiasule
				} else { // should focus jiasule
					chrome.tabs.update(umctab.id, {
						"highlighted": true
					}, function(umctab) {
					});
				}
			});
		} else { // tab not open
			chrome.tabs.create({
				"url": "http://www.jiasule.com/"
			}, function(tab) {
			});
		}
	});
}

/*
 * get request from page or popup
 */
function extensionRequestListener(request, sender, sendResponse) {
	console.debug(JSON.stringify(request));
	var resp = { 
		"action": request.action,
		"result": 0
	};
	if (request.action == 'focus_or_open_bu') {
		focusOrOpenBuTab(request.bu, function() {
		});
		sendResponse(resp);
	}
}

function onInstalledListener(info) {
	// extension install or update
	if ((info.reason != 'install') && (info.reason != 'update')) {
		return;
	}
	checkShowOptionsPage();
}

chrome.windows.onCreated.addListener(windowCreatedListener);
chrome.windows.onRemoved.addListener(windowRemovedListener);

chrome.browserAction.onClicked.addListener(popupClickedListener);

chrome.extension.onRequest.addListener(extensionRequestListener);

// chrome.management.onInstalled.addListener(onInstalledListener);
chrome.runtime.onInstalled.addListener(onInstalledListener);

console.debug(JSON.stringify(chrome.runtime.getManifest()));

})(window);
